<?php namespace Tazaq\FastTimetable\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use Illuminate\Http\Request;
use Tazaq\FastTimetable\Classes\Timetable;

class CLessons extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController'    ];
    
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Tazaq.FastTimetable', 'main_menu___header', 'main_menu___lessons');
    }

    /**
     * Получение расписания с датами
     * @param Request $request
     * @param string $optionType
     * @param string $slug
     * @return array
     */
    public function getLessons(Request $request, string $optionType, string $slug) {
        $data = $request->input();
        return Timetable::getLessons(
          $optionType,
          $slug,
          $data['dateFrom'] ?? null,
          $data['dateTo'] ?? null
        );
    }
}
