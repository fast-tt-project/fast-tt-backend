<?php namespace Tazaq\FastTimetable\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use Illuminate\Http\Request;
use Tazaq\FastTimetable\Classes\Timetable;
use Tazaq\FastTimetable\Models\MTemp_lesson;
use Tazaq\FastTimetable\Models\MTGC_item;

class CTemp_lessons extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController'    ];
    
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Tazaq.FastTimetable', 'main_menu___header', 'main_menu___temp_lessons');
    }


    /**
     * Получение расписания
     * @param Request $request
     * @param string $optionType
     * @param string $slug
     * @return array
     */
    public function getTempLessons(Request $request, string $optionType, string $slug) {
      return Timetable::getLessons($optionType, $slug);
    }
}
