<?php

namespace Tazaq\FastTimetable\Controllers;
use Illuminate\Http\Request;
use Backend\Classes\Controller;
use RainLab\Blog\Models\Post;

class CNews extends Controller
{
  public function getNews (Request $request) {
    return $this->getPaginatedPosts($request);
  }

  private function getPaginatedPosts(Request $request) {
    $data = $request->input();
    $page = isset($data["page"])
      ? (int) $data["page"]
      : 1;

    $per_page = isset($data["per_page"])
      ? (int) $data["per_page"]
      : 10;

    $posts = Post::with('featured_images')
      ->orderBy('published_at', 'desc')
      ->select('published_at', 'content', 'slug', 'title', 'metadata', 'id')
      ->remember(2)
      ->paginate($per_page, $page);

    return $posts;
  }
}
