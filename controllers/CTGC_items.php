<?php namespace Tazaq\FastTimetable\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use Tazaq\FastTimetable\Models\MTGC_item;
use Illuminate\Http\Request;

class CTGC_items extends Controller
{
  public $implement = ['Backend\Behaviors\ListController', 'Backend\Behaviors\FormController', 'Backend\Behaviors\ReorderController'];

  public $listConfig = 'config_list.yaml';
  public $formConfig = 'config_form.yaml';
  public $reorderConfig = 'config_reorder.yaml';

  public function __construct()
  {
    parent::__construct();
    BackendMenu::setContext('Tazaq.FastTimetable', 'main_menu___header', 'main_menu___tgc_items');
  }

  /**
   * Список преподов
   * @return array|\October\Rain\Database\Collection|MTGC_item
   */
  public function getListTeachers(Request $request = null, $full = null)
  {
    return MTGC_item::getList(1, !$full);
  }

  /**
   * Список групп
   * @param Request $request
   * @param null $full
   * @return array|\October\Rain\Database\Collection|MTGC_item
   */
  public function getListGroups(Request $request = null, $full = null)
  {
    return MTGC_item::getList(2, !$full);
  }

  /**
   * Список аудиторий ($full === 0 для получения всех аудиторий)
   * @return array|\October\Rain\Database\Collection|MTGC_item
   */
  public function getListClassrooms(Request $request = null, $full = null)
  {
    return MTGC_item::getList(3, 0);
  }

  /**
   * Вернёт все списки выше
   *
   * @param Request $request
   * @param null $full
   * @return array
   */
  public function getListAll(Request $request, $full = null)
  {
    return [
      'teachers' => $this->getListTeachers($request, $full),
      'groups' => $this->getListGroups($request, $full),
      'classrooms' => $this->getListClassrooms($request, $full)
    ];
  }
}
