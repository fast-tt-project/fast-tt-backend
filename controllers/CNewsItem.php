<?php

namespace Tazaq\FastTimetable\Controllers;
use Backend\Classes\Controller;
use RainLab\Blog\Models\Post;
use Illuminate\Http\Request;

class CNewsItem extends Controller
{
  public function getNewsItem(Request $request, string $slug) {
    return $this->getNewsPost($slug);
  }

  private function getNewsPost(string $slug) {
    return Post::with('featured_images')
      ->select('published_at', 'content', 'slug', 'title', 'metadata', 'id')
      ->where('slug', $slug)
      ->remember(2)
      ->first();
  }
}
