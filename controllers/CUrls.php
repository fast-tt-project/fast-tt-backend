<?php namespace Tazaq\FastTimetable\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use Tazaq\FastTimetable\Models\MUrl;

class CUrls extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController',        'Backend\Behaviors\FormController',        'Backend\Behaviors\ReorderController'    ];

    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $reorderConfig = 'config_reorder.yaml';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Tazaq.FastTimetable', 'main_menu___header_2', 'manage_menu___urls');
    }

    public function getUrls() {
        return MUrl::get(['slug', 'name']);
    }
}
