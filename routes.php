<?php

use Tazaq\FastTimetable\Classes\Api\BlogPostCreator;
use Illuminate\Http\Request;

$API_GET = '/api/v3/get/';
$API_POST = '/api/v3/post/';

// списки
Route::get($API_GET . 'teachers/{full?}',
  'tazaq\fasttimetable\controllers\CTGC_items@getListTeachers'
);
Route::get($API_GET . 'groups/{full?}',
  'tazaq\fasttimetable\controllers\CTGC_items@getListGroups'
);
Route::get($API_GET . 'classrooms/{full?}',
  'tazaq\fasttimetable\controllers\CTGC_items@getListClassrooms'
);
Route::get($API_GET . 'list-all/{full?}',
  'tazaq\fasttimetable\controllers\CTGC_items@getListAll'
);


// расписание
Route::get($API_GET . 'timetable/{optionType}/{slug}',
  'tazaq\fasttimetable\controllers\CTemp_lessons@getTempLessons'
);

Route::post($API_POST . 'timetable/{optionType}/{slug}',
  'tazaq\fasttimetable\controllers\CLessons@getLessons'
);

// news
Route::get($API_GET . 'news',
  'tazaq\fasttimetable\controllers\CNews@getNews'
);

Route::get($API_GET . 'news/{slug}',
  'tazaq\fasttimetable\controllers\CNewsItem@getNewsItem'
);

// vk-posts
Route::post('/api/vk/poster', function (Request $request) {
  $data = $request->input();
  if (! ($data['group_id'] == '130630740' && $data['secret'] === 'vkfastttposter') ) {
    exit('ok');
  };

  if ($data['type'] === 'confirmation') {
    echo '3cedd539';
  } elseif ($data['type'] === 'wall_post_new') {
    $post_creator = new BlogPostCreator();
    $post_creator->createPost($data['object']);

    echo 'ok';
  } else {
    echo 'ok';
  }
});

// vk-populate
Route::post('/api/vk/poster-populate', function (Request $request) {
  $data = $request->input();
  $post_creator = new BlogPostCreator();
  $post_creator->createPost($data);

  echo 'ok';
});
