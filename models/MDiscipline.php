<?php namespace Tazaq\FastTimetable\Models;

use Model;

/**
 * Model
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $description
 * @property string|null $metadata
 * @method static \October\Rain\Database\Collection|static[] all($columns = ['*'])
 * @method static \October\Rain\Database\Collection|static[] get($columns = ['*'])
 * @method static \October\Rain\Database\Builder|MDiscipline newModelQuery()
 * @method static \October\Rain\Database\Builder|MDiscipline newQuery()
 * @method static \October\Rain\Database\Builder|MDiscipline query()
 * @method static \Illuminate\Database\Eloquent\Builder|MDiscipline whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MDiscipline whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MDiscipline whereMetadata($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MDiscipline whereName($value)
 * @mixin \Eloquent
 */
class MDiscipline extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'tazaq_fasttimetable_disciplines';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
}
