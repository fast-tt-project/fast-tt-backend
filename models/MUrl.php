<?php namespace Tazaq\FastTimetable\Models;

use Model;

/**
 * Model
 */
class MUrl extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];


    /**
     * @var string The database table used by the model.
     */
    public $table = 'tazaq_fasttimetable_urls';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
}
