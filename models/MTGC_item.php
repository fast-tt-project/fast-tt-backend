<?php namespace Tazaq\FastTimetable\Models;

use Model;

/**
 * Model
 *
 * @property int $id
 * @property int $option_type
 * @property string|null $name
 * @property string $slug
 * @property string|null $description
 * @property string|null $metadata
 * @method static \October\Rain\Database\Collection|static[] all($columns = ['*'])
 * @method static \October\Rain\Database\Collection|static[] get($columns = ['*'])
 * @method static \October\Rain\Database\Builder|MTGC_item newModelQuery()
 * @method static \October\Rain\Database\Builder|MTGC_item newQuery()
 * @method static \October\Rain\Database\Builder|MTGC_item query()
 * @method static \Illuminate\Database\Eloquent\Builder|MTGC_item whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MTGC_item whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MTGC_item whereMetadata($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MTGC_item whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MTGC_item whereOptionType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MTGC_item whereSlug($value)
 * @mixin \Eloquent
 * @property string|null $alphabet
 * @property int $is_visible
 * @method static \Illuminate\Database\Eloquent\Builder|MTGC_item whereAlphabet($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MTGC_item whereIsVisible($value)
 */
class MTGC_item extends Model
{
  use \October\Rain\Database\Traits\Validation;

  /*
   * Disable timestamps by default.
   * Remove this line if timestamps are defined in the database table.
   */
  public $timestamps = false;


  /**
   * @var string The database table used by the model.
   */
  public $table = 'tazaq_fasttimetable_tgc_items';

  /**
   * @var array Validation rules
   */
  public $rules = [
  ];


  /**
   * Трансформация под vuetify autocomplete https://vuetifyjs.com/en/components/autocompletes/#state-selector
   *
   * @param $data
   * @return array
   */
  private static function transformToVuetifyArray($data)
  {
    $newData = [];
    $keys = array_keys($data);

    foreach ($keys as $key) {
      $newData[] = ['header' => $key];

      foreach ($data[$key] as $listItem) {
        $newData[] = $listItem;
      }
    }

    return $newData;
  }


  /**
   * Форматирует списки по первой букве, по группе, по корпусом
   * @param $data
   * @param $option_type
   * @return array
   */
  private static function alphabetFormat($data, $option_type)
  {
    $newData = [];
    if ($option_type === 1) {
      foreach ($data as &$item) {
        $eName = explode(' ', $item->name);
        $newName = $eName[0] . ' ' . mb_substr($eName[1], 0, 1);
        $newName .= count($eName) === 3 ? mb_substr($eName[2], 0, 1) : '';
        $item->name = $newName;
        $letter = mb_substr($item->name, 0, 1);
        $newData[$letter][] = $item;
      }

    } elseif ($option_type === 2) {
      foreach ($data as &$item) {
        $item->name = str_replace(' ', '', $item->name);
        $group = mb_substr($item->name, 0, mb_strpos($item->name, '-'));
        $newData[$group][] = $item;
      }

    } else {
      foreach ($data as &$item) {
        unset($item->is_visible);
        if ($item->slug === 'sportzal') {
          $newData['Корпус 3'][] = $item;
        } elseif ($item->slug === 'distants') {
          continue;
        } elseif ($item->slug === 'zal-1' || $item->slug === 'zal-2' || $item->slug < 100) {
          $newData['Корпус 1'][] = $item;
        } else {
          $newData['Корпус 2'][] = $item;
        }
      }
    }

    return $newData;
  }

  /**
   * Возвращает список $option_type
   * @param int $option_type
   * @param int $full
   * @return array|\October\Rain\Database\Collection|static
   */
  public static function getList($option_type, $full = 1)
  {
    $orderBy = $option_type === 3
      ? 'alphabet'
      : 'name';

    return self::transformToVuetifyArray(
      self::alphabetFormat(
        self::whereOptionType($option_type)
          ->where('is_visible', '>=', $full)
          ->where('slug', '<>', '')
          ->orderBy($orderBy)
          ->remember(30)
          ->get(['slug', 'name', 'is_visible']),
        $option_type
      )
    );
  }
}
