<?php namespace Tazaq\FastTimetable\Models;

use Model;

/**
 * Model
 *
 * @property int $id
 * @property int $weekday_id
 * @property int $discipline_id
 * @property int $discipline_type_id
 * @property int $teacher_id
 * @property int $group_id
 * @property int $classroom_id
 * @property string|null $l_date
 * @property string|null $l_begin
 * @property string|null $l_end
 * @property int|null $l_num
 * @method static \October\Rain\Database\Collection|static[] all($columns = ['*'])
 * @method static \October\Rain\Database\Collection|static[] get($columns = ['*'])
 * @method static \October\Rain\Database\Builder|MLesson newModelQuery()
 * @method static \October\Rain\Database\Builder|MLesson newQuery()
 * @method static \October\Rain\Database\Builder|MLesson query()
 * @method static \Illuminate\Database\Eloquent\Builder|MLesson whereClassroomId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MLesson whereDisciplineId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MLesson whereDisciplineTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MLesson whereGroupId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MLesson whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MLesson whereLBegin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MLesson whereLDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MLesson whereLEnd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MLesson whereLNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MLesson whereTeacherId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MLesson whereWeekdayId($value)
 * @mixin \Eloquent
 * @property string $hash_t
 * @method static \Illuminate\Database\Eloquent\Builder|MLesson whereHashT($value)
 */
class MLesson extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'tazaq_fasttimetable_lessons';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /**
     * Связи
     */
    public $belongsTo = [
        'weekday' => 'Tazaq\fasttimetable\Models\MWeekday',
        'discipline' => 'Tazaq\fasttimetable\Models\MDiscipline',
        'discipline_type' => 'Tazaq\fasttimetable\Models\MDiscipline_type',
        'teacher' => 'Tazaq\fasttimetable\Models\MTGC_item',
        'group' => 'Tazaq\fasttimetable\Models\MTGC_item',
        'classroom' => 'Tazaq\fasttimetable\Models\MTGC_item',
    ];
}
