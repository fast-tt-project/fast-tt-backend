<?php namespace Tazaq\FastTimetable\Models;

use Model;

/**
 * Model
 *
 * @property int $id
 * @property string $name
 * @property string $slug
 * @method static \October\Rain\Database\Collection|static[] all($columns = ['*'])
 * @method static \October\Rain\Database\Collection|static[] get($columns = ['*'])
 * @method static \October\Rain\Database\Builder|MWeekday newModelQuery()
 * @method static \October\Rain\Database\Builder|MWeekday newQuery()
 * @method static \October\Rain\Database\Builder|MWeekday query()
 * @method static \Illuminate\Database\Eloquent\Builder|MWeekday whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MWeekday whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MWeekday whereSlug($value)
 * @mixin \Eloquent
 */
class MWeekday extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'tazaq_fasttimetable_weekdays';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
}
