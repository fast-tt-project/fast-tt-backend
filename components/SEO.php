<?php namespace Tazaq\FastTimetable\Components;


use Tazaq\FastTimetable\Controllers\CTGC_items;
use Tazaq\FastTimetable\Controllers\CUrls;

class SEO {
    private const TAGLINE = 'Fast-tt.ru - быстрая альтернатива. ';
    private const UNIVERSITY = 'БФ ПНИПУ (Березниковский Филиал Пермского Национального Исследовательского Политехнического Университета) ';
    private const CITY = 'Находимся в городе Березники. ';
    private $SEO_STATIC;
    private $SEO_DYNAMIC;

    public function __construct()
    {
        $LIST_OF = function($name): string { return self::TAGLINE . self::CITY . "{$name} " . self::UNIVERSITY; };

        $this->SEO_STATIC = [
            'default' => [
                'name' => 'default',
                'title' => 'Fast-tt.ru - быстрое расписание занятий',
                'description' => $LIST_OF('Расписание занятий') . 'на сегодня и завтра, на неделю, списки преподавателей, групп, аудиторий. На сайте быстрого расписания присутствуют такие разделы как аудитории, где будет список аудиторий в университете, раздел преподавателей, где можно увидеть всех преподавателей в университете, раздел группы, где можно увидеть все группы университета, а также раздел пожертвований для пожертвований',
                'content' => [
                    'type' => 'links',
                    'startUrl' => '/',
                    'name' => 'Меню',
                    'data' => (new CUrls())->getUrls()
                ]
            ],
            'teachers' => [
                'name' => 'teachers',
                'title' => 'Преподаватели',
                'description' => $LIST_OF('Список преподавателей'),
                'content' => [
                    'type' => 'links',
                    'startUrl' => '/timetable/',
                    'name' => 'Преподаватели',
                    'data' => (new CTGC_items)->getListTeachers(null, false)
                ]
            ],
            'groups' => [
                'name' => 'groups',
                'title' => 'Группы',
                'description' => $LIST_OF('Список групп'),
                'content' => [
                    'type' => 'links',
                    'startUrl' => '/timetable/',
                    'name' => 'Группы',
                    'data' => (new CTGC_items)->getListGroups(null, false)
                ]
            ],
            'classrooms' => [
                'name' => 'classrooms',
                'title' => 'Аудитории',
                'description' => $LIST_OF('Список аудиторий'),
                'content' => [
                    'type' => 'links',
                    'startUrl' => '/timetable/',
                    'name' => 'Аудитории',
                    'data' => (new CTGC_items)->getListClassrooms(null, false)
                ]
            ]
        ];
    }

    public function getSEO($url) {
        $sliced_url = $this->partialUrl($url);
        if ($sliced_url[0] === 'timetable') {
            // TODO: реализовать динамические поля
            return ['title' => 'title', 'description' => 'description'];
        }

        return $this->SEO_STATIC[$sliced_url[0]] ?? null;
    }

    private function partialUrl($url)  {
        return array_slice(explode('/', $url), 3);
    }
}
