<?php namespace Tazaq\FastTimetable\Components;

use Cms\Classes\ComponentBase;
use Tazaq\FastTimetable\Components\SEO;

class FastTTSSRAPI extends ComponentBase {

    public function componentDetails()
    {
        return [
            'name'        => 'FastTT SSR API',
            'description' => 'Компонент плагина для реализации SSR'
        ];
    }

    public $title;
    public $description;
    public $name;
    public $content;

    public function onRun() {
        $this->loadComponent();
    }

    protected function loadComponent() {
        $seoData = (new SEO())->getSEO($this->currentPageUrl());

        $this->title = $seoData['title'] ?? null;
        $this->description = $seoData['description'] ?? null;
        $this->name = $seoData['name'] ?? null;
        $this->content = $seoData['content'] ?? null;
    }
}
