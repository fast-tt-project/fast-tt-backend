<?php namespace Tazaq\FastTimetable;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
        return [
            '\Tazaq\Fasttimetable\Components\FastTTSSRAPI' => 'FastTTSSRAPI'
        ];
    }

    public function registerSettings()
    {
    }
}
