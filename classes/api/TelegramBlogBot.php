<?php
namespace Tazaq\FastTimetable\Classes\Api;
use Longman\TelegramBot\Exception\TelegramException;
use Longman\TelegramBot\Request;
use Longman\TelegramBot\Telegram;
use RainLab\Blog\Models\Post;
use File;

class TelegramBlogBot extends __TelegramBlogBotConfig implements ITelegramBlogBot
{
    private $t_bot;

    public function __construct()
    {
        try {
            $this->t_bot = new Telegram($this->bot_api_token, $this->bot_username);
        } catch (TelegramException $e) {
            echo $e;
        }
    }

    public function makeFromBlogPosts()
    {
        $posts = Post::with('featured_images')->orderByDesc('id')->limit(1)->get();
        foreach ($posts as $blogPost) {

            $media = [];
            if ($images = $blogPost->featured_images) {
                foreach ($images as $image) {
                    $media[] = [
                        'type' => 'photo',
                        'media' => $image->getPath() ?? null,
                        'caption' => $blogPost->title
                    ];
                }
            }

            $metadata = json_decode($blogPost->metadata);
            $text = strip_tags($blogPost->title . "\n\n" . $blogPost->content) . "\n\n" . $metadata->vk_public_url;
            if ($media) {
                echo $this->sendMediaGroupToChanel($media);
            }
            if ($text) {
                echo $this->sendMessageToChanel($text);
            }
        }
    }

    public function sendMessageToChanel($text)
    {
        try {
            return Request::sendMessage([
                'chat_id' => $this->chat_id,
                'text' => $text
            ]);
        } catch (TelegramException $e) {
            return $e;
        }
    }

    public function sendMediaGroupToChanel($media)
    {
        try {
            return Request::sendMediaGroup([
                'chat_id' => $this->chat_id,
                'media' => $media
            ]);
        } catch (TelegramException $e) {
            return $e;
        }
    }
}
