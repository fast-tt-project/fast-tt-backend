<?php
namespace Tazaq\FastTimetable\Classes\Api;
use Rainlab\blog\Models\Post;
use System\Models\File;

class BlogPostCreator implements IBlogPostCreator
{
    const post_title = 'Новости сообщества ВК #';
    const vk_group_name = 'raspisaniebfpnipu';

    private $vk_public_url;
    private $vk_post_author;

    public function createPost($data)
    {
        $owner_id = abs($data['owner_id']);
        $this->vk_public_url = "https://vk.com/public{$owner_id}?w=wall-{$owner_id}_{$data['id']}";

        $this->vk_post_author = $this->makeAuthor($data);
        $content = $this->makeContent($data);
        $post = $this->makePost($data, $content);

        if (isset($data['attachments'])) {
            $this->attachToPost($post, $data['attachments']);
        }

        if (isset($data['copy_history'])) {
            $post->content .= '<p>***Репост записи***</p>';
            $post->save();

            $reposted_post = new BlogPostCreator();
            $reposted_post->createPost($data['copy_history'][0]);
        }
    }

    private function makeContent($data) {
        return "<div>{$data['text']}</div>";
    }

    private function makeAuthor($data) {
        $signer_id =  isset($data['signer_id']) ? 'id' . $data['signer_id'] : self::vk_group_name;
        $signer_id = 'https://vk.com/' . $signer_id;
        return $signer_id;
    }

    private function makePost($data, $content) {
        $post = new Post();
        $post->title = self::post_title . $data['id'];
        $post->content = $content;
        $post->slug =  'p' . $data['owner_id'] . '-' . $data['id'];
        $post->published = 1;
        $post->published_at = date('Y-m-d H:i:s', $data['date']);
        $post->user_id = 1;
        $post->metadata = json_encode(['vk_public_url' => $this->vk_public_url, 'vk_post_author' => $this->vk_post_author]);
        $post->save();

        return $post;
    }

    private function attachToPost(Post $post, $attachments) {
        foreach ($attachments as $attachment) {
            $a_type = $attachment['type'];
            if ($a_type == 'photo') {
                $last_size_url = $attachment[$a_type]['sizes'][count($attachment[$a_type]['sizes']) - 1]['url'];
                $file = new File();
                $file->fromUrl($last_size_url);
                $post->featured_images()->add($file);

            } elseif ($a_type == 'video') {
                $url = $this->vk_public_url;
                $video_attach = "<div><a href='{$url}' target='_blank'>Посмотреть видео</a></div>";
                $post->content .= $video_attach;
                $post->save();

            } elseif ($a_type == 'audio') {
                $url = $this->vk_public_url;
                $audio_attach = "<div><a href='{$url}' target='_blank'>Прослушать аудио</a></div>";
                $post->content .= $audio_attach;
                $post->save();

            } elseif ($a_type == 'doc') {
                $url = $attachment[$a_type]['url'];
                $title = $attachment[$a_type]['title'];
                $doc_attach = "<div><a href='{$url}' target='_blank'>{$title}</a></div>";
                $post->content .= $doc_attach;
                $post->save();

            } elseif ($a_type == 'link') {
                $url = $attachment[$a_type]['url'];
                $title = $attachment[$a_type]['title'];
                $link_attach = "<div><a href='{$url}' target='_blank'>{$title}</a></div>";
                $post->content .= $link_attach;
                $post->save();

            } elseif ($a_type == 'poll') {
                $url = $this->vk_public_url;
                $poll_attach = "<div><a href='{$url}' target='_blank'>Перейти к опросу</a></div>";

                $question = $attachment[$a_type]['question'];
                $votes = $attachment[$a_type]['votes'];
                $pool_self = "<h6>{$question}</h6>";
                $pool_self .= "<p>{$votes} человек проголосовало</p>";
                $pool_self .= '<ul class="card ft_pad_sm">';
                $answers = $attachment[$a_type]['answers'];
                if (count($answers)) {
                    foreach ($answers as $answer) {
                        $pool_self .= "<li>{$answer['text']} ({$answer['votes']}) - {$answer['rate']}%</li>";
                    }
                }
                $pool_self .= '</ul>';

                $post->content .= $poll_attach;
                $post->content .= $pool_self;
                $post->save();
            }
        }
    }
}
