<?php


namespace Tazaq\FastTimetable\Classes\Api;


interface ITelegramBlogBot {
    public function makeFromBlogPosts();
    public function sendMessageToChanel($data);
    public function sendMediaGroupToChanel($data);
}
