<?php
namespace Tazaq\FastTimetable\Classes\Api;

interface IBlogPostCreator {
    public function createPost($data);
}
