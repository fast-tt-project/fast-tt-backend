<?php namespace Tazaq\FastTimetable\Classes;


use Tazaq\FastTimetable\Models\MLesson;
use Tazaq\FastTimetable\Models\MTemp_lesson;
use Tazaq\FastTimetable\Models\MTGC_item;

class Timetable
{
  const OPTION_TYPE_NUM = [
    'teachers' => 1,
    'groups' => 2,
    'classrooms' => 3,
  ];

  /**
   * Удаление лишней инфы
   * @param $lessons
   * @return mixed
   */
  public static function lessonMapper($lessons)
  {
    return $lessons->map(function ($x) {
      return [
        'weekday' => $x['weekday']['name'],
        'discipline' => $x['discipline']['name'],
        'discipline_type' => $x['discipline_type']['name'],
        'teacher' => ['name' => $x['teacher']['name'], 'slug' => $x['teacher']['slug']],
        'group' => ['name' => $x['group']['name'], 'slug' => $x['group']['slug']],
        'classroom' => ['name' => $x['classroom']['name'], 'slug' => $x['classroom']['slug']],
        'l_date' => $x['l_date'],
        'l_begin' => $x['l_begin'],
        'l_end' => $x['l_end'],
        'l_num' => $x['l_num']
      ];
    });
  }


  /**
   * Создаётся массив расписания сгруппированное по неделям, затем по дням
   * @param $lessons - массив занятий
   */
  public static function getWeekWithDaysFormat($lessons = [])
  {
    if (!$lessons) return [];

    // номер недели по дате
    $week = function ($date) {
      return date('W', strtotime($date));
    };

    // конвертация строк расписания в массив
    $converted_timetable = [];
    $week_num = -1;
    $current_week = -1;
    foreach ($lessons as $lesson) {
      $lesson_date = $lesson['l_date'];
      if (($cur_week = $week($lesson_date)) != $current_week) {
        $week_num++;
        $converted_timetable[$week_num] = [];
        $current_week = $cur_week;
      }

      $converted_timetable[$week_num][$lesson_date][] = $lesson;
    }

    return $converted_timetable;
  }

  /**
   * Конструктор запроса к базе
   * @param MTemp_lesson | MLesson $lessons
   * @param string $optionType
   * @param null $dateFrom
   * @param null $dateTo
   */
  private static function makeLessons($lessons, $dateFrom = null, $dateTo = null) {
    if (!$lessons) return [];

    $lessons = $lessons
      ->with(['weekday' => function($q) {$q->select(['id', 'name']);}])
      ->with(['discipline' => function($q) {$q->select(['id', 'name']);}])
      ->with(['discipline_type' => function($q) {$q->select(['id', 'name']);}])
      ->with(['teacher' => function($q) {$q->select(['id', 'name', 'slug']);}])
      ->with(['group' => function($q) {$q->select(['id', 'name', 'slug']);}])
      ->with(['classroom' => function($q) {$q->select(['id', 'name', 'slug']);}])
      ->whereBetween('l_date', [$dateFrom, $dateTo])
      ->orderBy('l_date')
      ->orderBy('l_begin')
      ->remember(10)
      ->get();

    return Timetable::getWeekWithDaysFormat(Timetable::lessonMapper($lessons));
  }

  private static function makeMappedLessons($lessons, $dateFrom = null, $dateTo = null) {
    if (!$lessons) return [];

    $lessons = $lessons
      ->with(['weekday' => function($q) {$q->select(['id', 'name']);}])
      ->with(['discipline' => function($q) {$q->select(['id', 'name']);}])
      ->with(['discipline_type' => function($q) {$q->select(['id', 'name']);}])
      ->with(['teacher' => function($q) {$q->select(['id', 'name', 'slug']);}])
      ->with(['group' => function($q) {$q->select(['id', 'name', 'slug']);}])
      ->with(['classroom' => function($q) {$q->select(['id', 'name', 'slug']);}])
      ->whereBetween('l_date', [$dateFrom, $dateTo])
      ->orderBy('l_date')
      ->orderBy('l_begin')
      ->remember(10)
      ->get();

    return Timetable::lessonMapper($lessons);
  }


  /**
   * @param string $optionType
   * @param string $slug
   * @param null $dateFrom
   * @param null $dateTo
   */
  public static function getLessons(string $optionType, string $slug, $dateFrom = null, $dateTo = null)
  {
    $Model = function($Model, string $optionType, int $id) {
      switch (mb_strtolower($optionType)) {
        case 'teachers': return $Model::whereTeacherId($id);
        case 'groups': return $Model::whereGroupId($id);
        case 'classrooms': return $Model::whereClassroomId($id);
        default: return null;
      }
    };


    $tgcItemModel = MTGC_item::whereOptionType(self::OPTION_TYPE_NUM[$optionType])
      ->whereSlug($slug)
      ->first(['id', 'name']);

    // 404
    if (!isset($tgcItemModel)) {
      return \Response::make('Page not found', 404);
    }

    if ($tgcItemModel->id ?? null) {
      $today = date('Y-m-d');
      $myDeathDate = '2999-12-31';
      $lessonModel = new MLesson();
      $tempLessonModel = new MTemp_lesson();

      if (!$dateFrom && !$dateTo) {
        // если нет дат, то все занятия из $tempLessonModel
        return [
          'tgcItemName' => $tgcItemModel->name,
          'lessons' => self::makeLessons(
            $Model($tempLessonModel, $optionType, $tgcItemModel->id),
            $today,
            $myDeathDate
          ),
          'optionType' => $optionType
        ];

      } else {
        if (!$dateFrom) $dateFrom = $today;
        if (!$dateTo) $dateTo = $myDeathDate;

        $_lessons = self::makeMappedLessons(
          $Model($lessonModel, $optionType, $tgcItemModel->id),
          $dateFrom,
          $dateTo
        );
        $_tempLessons = self::makeMappedLessons(
          $Model($tempLessonModel, $optionType, $tgcItemModel->id),
          $dateFrom,
          $dateTo
        );

        $resultLessons = [];
        foreach ($_lessons as $lesson) {
          $resultLessons[] = $lesson;
        }

        foreach ($_tempLessons as $lesson) {
          $resultLessons[] = $lesson;
        }

        return [
          'tgcItemName' => $tgcItemModel->name,
          'lessons' => Timetable::getWeekWithDaysFormat($resultLessons),
          'optionType' => $optionType,
        ];
      }
    }

    return [
      'tgcItemName' => $tgcItemModel->name,
      'lessons' => [],
      'optionType' => null
    ];
  }
}
