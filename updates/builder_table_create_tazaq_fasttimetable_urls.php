<?php namespace Tazaq\FastTimetable\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateTazaqFasttimetableUrls extends Migration
{
    public function up()
    {
        Schema::create('tazaq_fasttimetable_urls', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->string('slug')->unique();
            $table->string('name');
        });
    }

    public function down()
    {
        Schema::dropIfExists('tazaq_fasttimetable_urls');
    }
}

