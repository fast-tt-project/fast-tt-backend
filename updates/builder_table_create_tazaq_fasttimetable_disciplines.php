<?php namespace Tazaq\FastTimetable\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateTazaqFasttimetableDisciplines extends Migration
{
    public function up()
    {
        Schema::create('tazaq_fasttimetable_disciplines', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('name')->nullable();
            $table->text('description')->nullable();
            $table->text('metadata')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('tazaq_fasttimetable_disciplines');
    }
}
