<?php namespace Tazaq\FastTimetable\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateTazaqFasttimetableTgcItems2 extends Migration
{
    public function up()
    {
        Schema::table('tazaq_fasttimetable_tgc_items', function($table)
        {
            $table->boolean('is_visible')->default(0);
            $table->dropColumn('deleted_at');
        });
    }
    
    public function down()
    {
        Schema::table('tazaq_fasttimetable_tgc_items', function($table)
        {
            $table->dropColumn('is_visible');
            $table->timestamp('deleted_at')->nullable();
        });
    }
}
