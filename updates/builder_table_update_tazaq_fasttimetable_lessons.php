<?php namespace Tazaq\FastTimetable\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateTazaqFasttimetableLessons extends Migration
{
    public function up()
    {
        Schema::table('tazaq_fasttimetable_lessons', function($table)
        {
            $table->renameColumn('hash_t', 'hash');
        });
    }
    
    public function down()
    {
        Schema::table('tazaq_fasttimetable_lessons', function($table)
        {
            $table->renameColumn('hash', 'hash_t');
        });
    }
}
