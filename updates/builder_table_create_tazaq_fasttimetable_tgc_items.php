<?php namespace Tazaq\FastTimetable\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateTazaqFasttimetableTgcItems extends Migration
{
    public function up()
    {
        Schema::create('tazaq_fasttimetable_tgc_items', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->timestamp('deleted_at')->nullable();
            $table->smallInteger('option_type')->default(1);
            $table->string('name')->nullable();
            $table->string('slug')->unique();
            $table->text('description')->nullable();
            $table->text('metadata')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('tazaq_fasttimetable_tgc_items');
    }
}