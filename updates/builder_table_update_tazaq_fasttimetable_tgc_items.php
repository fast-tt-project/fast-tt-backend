<?php namespace Tazaq\FastTimetable\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateTazaqFasttimetableTgcItems extends Migration
{
    public function up()
    {
        Schema::table('tazaq_fasttimetable_tgc_items', function($table)
        {
            $table->string('alphabet')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('tazaq_fasttimetable_tgc_items', function($table)
        {
            $table->dropColumn('alphabet');
        });
    }
}
