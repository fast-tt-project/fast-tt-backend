<?php namespace Tazaq\FastTimetable\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateTazaqFasttimetableLessons extends Migration
{
    public function up()
    {
        Schema::create('tazaq_fasttimetable_lessons', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('weekday_id')->unsigned()->default(1);
            $table->integer('discipline_id')->unsigned()->default(1);
            $table->integer('discipline_type_id')->unsigned()->default(1);
            $table->integer('teacher_id')->unsigned()->default(1);
            $table->integer('group_id')->unsigned()->default(1);
            $table->integer('classroom_id')->unsigned()->default(1);
            $table->date('l_date')->nullable();
            $table->time('l_begin')->nullable();
            $table->time('l_end')->nullable();
            $table->smallInteger('l_num')->nullable();
            $table->string('hash_t', 32)->unique();

            $table->foreign('weekday_id')->references('id')->on('tazaq_fasttimetable_weekdays');
            $table->foreign('discipline_id')->references('id')->on('tazaq_fasttimetable_disciplines');
            $table->foreign('discipline_type_id')->references('id')->on('tazaq_fasttimetable_discipline_types');
            $table->foreign('teacher_id')->references('id')->on('tazaq_fasttimetable_tgc_items');
            $table->foreign('group_id')->references('id')->on('tazaq_fasttimetable_tgc_items');
            $table->foreign('classroom_id')->references('id')->on('tazaq_fasttimetable_tgc_items');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('tazaq_fasttimetable_lessons');
    }
}