<?php

namespace Tazaq\FastTimetable\Updates;

use Seeder;
use Db;

class SeedTables extends Seeder {

    public function run () {
        // сидер для tgc
        Db::table('tazaq_fasttimetable_tgc_items')->insert([
            'name' => '',
            'slug' => '',
            'description' => 'Нулевая запись',
        ]);

        // сидер для weekdays
        Db::table('tazaq_fasttimetable_weekdays')->insert([
            'name' => '',
            'slug' => '',
        ]);

        // сидер для disciplines
        Db::table('tazaq_fasttimetable_disciplines')->insert([
            'name' => '',
            'description' => 'Нулевая запись',
        ]);

        // сидер для discipline_types
        Db::table('tazaq_fasttimetable_discipline_types')->insert([
            'name' => '',
            'description' => 'Нулевая запись',
        ]);
    }

}