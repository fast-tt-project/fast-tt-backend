<?php namespace Tazaq\FastTimetable\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateTazaqFasttimetableWeekdays extends Migration
{
    public function up()
    {
        Schema::create('tazaq_fasttimetable_weekdays', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('name');
            $table->string('slug')->index();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('tazaq_fasttimetable_weekdays');
    }
}
